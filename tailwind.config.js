/** @type {import('tailwindcss').Config} */
module.exports = {
  content: ["./src/**/*.{html,js}"],
  mode: 'jit',
  theme: {
    colors: {
      'oa-white':'#FFFFFF',
      'oa-primary': {
        5: '#F5F7FF',
        10: '#E7EAF9',
        20: '#C3C9EF',
        30: '#9AA7E4',
        40: '#4C69D2',
        50: '#1E4ECA',
        60: '#1646BF',
        70: '#003CB3',
        80: '#0031A7',
        90: '#001E94',
        100:'#000C3D'
      },
    },
    extend: {  
      fontFamily: {
        'soll': ["Soll", "sans-serif"],
      }
    },
  },
  plugins: [],
}
